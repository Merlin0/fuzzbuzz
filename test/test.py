from unittest import TestCase
from FizzBuzz import FizzBuzz


class TestFizzBuzz(TestCase):
    def test_FizzBuzz_equals_number(self):
        i = 2
        
        result = FizzBuzz(i)
        
        self.assertEqual(result, i)

    def test_FizzBuzz_equals_Fuzz(self):
        i = 3
        
        result = FizzBuzz(i)
        
        self.assertEqual(result, "Fizz")

    def test_FizzBuzz_equals_Buzz(self):
        i = 5
        
        result = FizzBuzz(i)
        
        self.assertEqual(result, "Buzz")

    def test_FizzBuzz_equals_FizzBuzz(self):
        i = 15

        result = FizzBuzz(i)

        self.assertEqual(result, "FizzBuzz")
